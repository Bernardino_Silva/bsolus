<?php


namespace bsolus;
require("Calculator.php");

/**
 * Class Main
 * @package bsolus
 */
class Main
{
    private $calculator;
    private $result;
    private $operation;

    public function __construct()
    {
        $this->operation = array();
        $argv = $_SERVER['argv'];
        unset($argv[0]); # remove first position of array
        $this->operation = $argv;
        $this->calculator = new Calculator();
        $this->readArgv($argv);
    }

    /**
     * Define operators
     * @param $idx
     * @return int
     */
    private function getOperators($idx)
    {
        $operators = array("+", "-", "x", "/");
        return $idx >= 0 ? $operators[$idx] : -1;
    }

    /**
     * Get result
     * @return float
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Get arithmetic operation
     * @return array
     */
    public function getOperationValues()
    {
        return $this->operation;
    }

    /**
     * Read argv - execute multiple operations
     * @param $argv
     */
    private function readArgv($argv)
    {
        $i = 0;
        if (count($argv) >= 3)
            foreach ($argv as $key => $value) {
                if ($i == 2) {
                    $argv[$key] = $this->execute($argv[$key - 2], $argv[$key - 1], $value);
                    $this->result = $argv[$key];
                    unset($argv[$key - 1]);
                    unset($argv[$key - 2]);
                    break;
                }
                ++$i;
            }

        count($argv) >= 3 ? $this->readArgv($argv) : null;
    }

    /**
     * Execute calculation function
     *
     * @param $valueA
     * @param $operator
     * @param $valueB
     * @return float
     */
    private function execute($valueA, $operator, $valueB)
    {
        switch ($operator) {
            case $this->getOperators(0):
                return $this->calculator->sum($valueA, $valueB);
                break;
            case $this->getOperators(1):
                return $this->calculator->subtract($valueA, $valueB);
                break;
            case $this->getOperators(2):
                return $this->calculator->multiply($valueA, $valueB);
                break;
            case $this->getOperators(3):
                return $this->calculator->divide($valueA, $valueB);
                break;
            default:
                return 0;
                break;
        }
    }
}

$main = new Main();
echo "Operation: " . implode(" ", $main->getOperationValues()) . "\n";
echo "Result: " . $main->getResult() . "\n";