<?php

namespace bsolus;
include ("ICalculator.php");

/**
 * Class Calculator
 * @author Bernardino Silva
 */
class Calculator implements ICalculator
{

    /**
     * @inheritDoc
     */
    public function sum($valueA, $valueB)
    {
        return $valueA !== null && $valueB !== null ? $valueA + $valueB : 0;
    }

    /**
     * @inheritDoc
     */
    public function subtract($valueA, $valueB)
    {
        return $valueA !== null && $valueB !== null ? $valueA - $valueB : 0;
    }

    /**
     * @inheritDoc
     */
    public function multiply($valueA, $valueB)
    {
        return $valueA !== null && $valueB !== null ? $valueA * $valueB : 0;
    }

    /**
     * @inheritDoc
     */
    public function divide($valueA, $valueB)
    {
        return $valueA !== null && $valueB !== null ? $valueA / $valueB : 0;
    }
}