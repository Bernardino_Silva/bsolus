# README #
# BSOLUS - Challenge #

## Instructions ##
1. Open project folder
2. Use the calculator via the command line

___

### Sum function: ###
```
php Main.php 4 + 3
```
### Result: 
```
Operation: 4 + 3                                                                                                                                                                                                                                      >
Result: 7
```
___
### Subtract function: ###
```
php Main.php 100 - 30
```
### Result: 
```
Operation: 100 - 30                                                                                                                                                                                                                                      >
Result: 70
```
***
### Multiply function: ###
```
php Main.php 100 x 30
```
### Result: 
```
Operation: 100 x 30                                                                                                                                                                                                                                      >
Result: 3000
```
***
### Division function: ###
```
php Main.php 100 / 30
```
### Result: 
```
Operation: 100 / 30                                                                                                                                                                                                                                      >
Result: 3.3333333333333
```
### Multiple operations: ###
```
php Main.php 100 - 30 + 20 + 10
```
### Result: 
```
Operation: 100 - 30 + 20 + 10
Result: 100
```
       
    

  
