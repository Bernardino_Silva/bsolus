<?php

namespace bsolus;

/**
 * Interface ICalculator
 * @author Bernardino Silva
 */
interface ICalculator
{
    /**
     * Obtain the result of the sum given 2 values
     *
     * @param $valueA
     * @param $valueB
     * @return mixed
     */
    public function sum($valueA, $valueB);

    /**
     * Obtain the result of the subtraction given 2 values
     *
     * @param $valueA
     * @param $valueB
     * @return mixed
     */
    public function subtract($valueA, $valueB);

    /**
     * Obtain the result of the multiplication given 2 values
     *
     * @param $valueA
     * @param $valueB
     * @return mixed
     */
    public function multiply($valueA, $valueB);

    /**
     * Obtain the result of the division given 2 values
     *
     * @param $valueA
     * @param $valueB
     * @return mixed
     */
    public function divide($valueA, $valueB);

}